import { Plugin } from 'obsidian';

declare class TaskbonePlugin extends Plugin {
    onload(): Promise<void>;
    onunload(): Promise<void>;
}

export { TaskbonePlugin as default };
