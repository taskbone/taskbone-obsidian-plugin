import typescript from '@rollup/plugin-typescript';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import cleanup from 'rollup-plugin-cleanup';
import json from '@rollup/plugin-json';
import { dts } from "rollup-plugin-dts";

const banner =
  `/*
THIS IS A GENERATED/BUNDLED FILE BY ROLLUP
if you want to view the source visit the plugins repository
*/
`;

export default [
  {
    // path to your declaration files root
    input: 'src/main.ts',
    output: {
      dir: 'dist'
    },
    plugins: [
      dts()
    ]
  },
  {
    input: 'src/main.ts',
    output: {
      dir: 'dist',
      sourcemap: 'inline',
      format: 'cjs',
      exports: 'default',
      banner,
    },
    external: ['obsidian'],
    plugins: [
      typescript(),
      json(),
      nodeResolve({ browser: true }),
      commonjs(),
      cleanup({ comments: 'none' }),
    ]
  }
]